#!/bin/bash

parameters="${1}${2}${3}${4}${5}${6}${7}${8}${9}"

Escape_Variables()
{
	text_progress="\033[38;5;113m"
	text_success="\033[38;5;113m"
	text_warning="\033[38;5;221m"
	text_error="\033[38;5;203m"
	text_message="\033[38;5;75m"

	text_bold="\033[1m"
	text_faint="\033[2m"
	text_italic="\033[3m"
	text_underline="\033[4m"

	erase_style="\033[0m"
	erase_line="\033[0K"

	move_up="\033[1A"
	move_down="\033[1B"
	move_foward="\033[1C"
	move_backward="\033[1D"
}

Parameter_Variables()
{
	if [[ $parameters == *"-v"* || $parameters == *"-verbose"* ]]; then
		verbose="1"
		set -x
	fi
}

Path_Variables()
{
	script_path="${0}"
	directory_path="${0%/*}"

	resources_path="$directory_path/resources"

	if [[ -d /usr/local/opt/openprebootmedia ]]; then
		resources_path="/usr/local/opt/openprebootmedia"
	fi
}

Input_Off()
{
	stty -echo
}

Input_On()
{
	stty echo
}

Output_Off()
{
	if [[ $verbose == "1" ]]; then
		"$@"
	else
		"$@" &>/dev/null
	fi
}

Check_Environment()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking system environment."${erase_style}

	if [ -d /Install\ *.app ]; then
		environment="installer"
	fi

	if [ ! -d /Install\ *.app ]; then
		environment="system"
	fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked system environment."${erase_style}
}

Check_Root()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking for root permissions."${erase_style}

	if [[ $environment == "installer" ]]; then
		root_check="passed"
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
	else

		if [[ $(whoami) == "root" && $environment == "system" ]]; then
			root_check="passed"
			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Root permissions check passed."${erase_style}
		fi

		if [[ ! $(whoami) == "root" && $environment == "system" ]]; then
			root_check="failed"
			echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Root permissions check failed."${erase_style}
			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with root permissions."${erase_style}

			Input_On
			exit
		fi

	fi
}

Check_SIP()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking System Integrity Protection status."${erase_style}

	if [[ $(csrutil status | grep status) == *disabled* ]] || [[ $(csrutil status | grep status) == *Custom\ Configuration* && $(csrutil status | grep "Kext Signing") == *disabled* ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ System Integrity Protection status check passed."${erase_style}
	fi

	if [[ $(csrutil status | grep status) == *enabled* && ! $(csrutil status | grep status) == *Custom\ Configuration* ]] || [[ $(csrutil status | grep status) == *Custom\ Configuration* && $(csrutil status | grep "Kext Signing") == *enabled* ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- System Integrity Protection status check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with System Integrity Protection disabled."${erase_style}

		Input_On
		exit
	fi
}

Check_Installer()
{
	if [[ "$environment" == "installer" ]]; then
		installer_volume_name="$(diskutil info /|grep "Volume Name"|sed 's/.*\ //')"
		installer_volume_path="/Volumes/$installer_volume_name"

		if [[ -e "$installer_volume_path"/BaseSystem.dmg ]]; then
			installer_images_path="$installer_volume_path"
		else
			Input_Installer
		fi

		if [[ -e "$installer_volume_path"/System/Installation/Packages/Core.pkg ]]; then
			installer_packages_path="$installer_volume_path/System/Installation/Packages"
		else
			Input_Installer
		fi
	else
		Input_Installer
	fi
}

Input_Installer()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What installer would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input an installer path."${erase_style}

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " installer_application_path
	Input_Off

	installer_application_name="${installer_application_path##*/}"
	installer_application_name_partial="${installer_application_name%.app}"

	installer_sharedsupport_path="$installer_application_path/Contents/SharedSupport"
}

Check_Installer_Stucture()
{
	Output_Off hdiutil attach "$installer_sharedsupport_path"/InstallESD.dmg -mountpoint /tmp/InstallESD -nobrowse -noverify

	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking installer structure."${erase_style}

		if [[ -e /tmp/InstallESD/BaseSystem.dmg ]]; then
			installer_images_path="/tmp/InstallESD"
		fi
		if [[ -e "$installer_sharedsupport_path"/BaseSystem.dmg ]]; then
			installer_images_path="$installer_sharedsupport_path"
		fi
		if [[ -e /tmp/InstallESD/Packages/Core.pkg ]]; then
			installer_packages_path="/tmp/InstallESD/Packages"
		fi

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked installer structure."${erase_style}


	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Mounting installer disk images."${erase_style}

		Output_Off hdiutil attach "$installer_images_path"/BaseSystem.dmg -mountpoint /tmp/Base\ System -nobrowse -noverify

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Mounted installer disk images."${erase_style}
}

Check_Installer_Version()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking installer version."${erase_style}

		installer_version="$(defaults read /tmp/Base\ System/System/Library/CoreServices/SystemVersion.plist ProductVersion)"
		installer_version_short="$(defaults read /tmp/Base\ System/System/Library/CoreServices/SystemVersion.plist ProductVersion | cut -c-5)"

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Checked installer version."${erase_style}	
}

Check_Installer_Support()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Unmounting installer disk images."${erase_style}

		Output_Off hdiutil detach /tmp/InstallESD
		Output_Off hdiutil detach /tmp/Base\ System

	echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Unmounted installer disk images."${erase_style}

	
	echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Checking installer support."${erase_style}

	if [[ $installer_version_short == "10.1"[3-5] ]]; then
		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Installer support check passed."${erase_style}
	else
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Installer support check failed."${erase_style}
		echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Run this tool with a supported installer."${erase_style}

		Input_On
		exit
	fi
}

Input_Volume()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ What volume would you like to use?"${erase_style}
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Input a volume number."${erase_style}

	for volume_path in /Volumes/*; do
		volume_name="${volume_path#/Volumes/}"

		if [[ ! "$volume_name" == com.apple* ]]; then
			volume_number=$(($volume_number + 1))
			declare volume_$volume_number="$volume_name"

			echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/     ${volume_number} - ${volume_name}"${erase_style} | sort
		fi

	done

	Input_On
	read -e -p "$(date "+%b %d %H:%M:%S") / " volume_number
	Input_Off

	volume="volume_$volume_number"
	volume_name="${!volume}"
	volume_path="/Volumes/$volume_name"
}

Create_Preboot_Media()
{
	disk_identifier="$(diskutil info "$volume_name"|grep "Device Identifier"|sed 's/.*\ //')"
	disk_identifier_whole="$(diskutil info "$volume_name"|grep "Part of Whole"|sed 's/.*\ //')"

	if [[ "$(diskutil info "$volume_name"|grep "File System Personality"|sed 's/.*\ //')" == "APFS" ]]; then

		preboot_identifier="$(diskutil info "$volume_name"|grep "Booter Disk"|sed 's/.*\ //')"
		preboot_folder="$(diskutil info "$volume_name"|grep "Volume UUID"|sed 's/.*\ //')"

		if [[ ! -z "$preboot_identifier" ]]; then
			echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Removing existing Preboot folder."${erase_style}

				Output_Off diskutil mount "$preboot_identifier"
				chflags nouchg /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices/boot.efi
				Output_Off rm -R /Volumes/Preboot/"$preboot_folder"

			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Removed existing Preboot folder."${erase_style}
		else
			echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Creating Preboot partition."${erase_style}

				Output_Off diskutil apfs addVolume "$disk_identifier_whole" APFS Preboot -role B

			echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Created Preboot partition."${erase_style}
		fi

		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Creating Preboot folders."${erase_style}

			mkdir -p /Volumes/Preboot/"$preboot_folder"/Library/Preferences/SystemConfiguration
			mkdir -p /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Created Preboot folders."${erase_style}


		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Copying Preboot files."${erase_style}

			cp "$volume_path"/System/Library/CoreServices/boot.efi* /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices
			cp "$volume_path"/System/Library/CoreServices/SystemVersion.plist /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices
			Output_Off cp "$volume_path"/System/Library/CoreServices/PlatformSupport.plist /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices
			
			cp "$volume_path"/Library/Preferences/SystemConfiguration/com.apple.Boot.plist /Volumes/Preboot/"$preboot_folder"/Library/Preferences/SystemConfiguration

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Copied Preboot files."${erase_style}


		echo -e $(date "+%b %d %H:%M:%S") ${text_progress}"> Creating Preboot files."${erase_style}

			echo "$preboot_folder" > /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices/.root_uuid

		echo -e $(date "+%b %d %H:%M:%S") ${move_up}${erase_line}${text_success}"+ Created Preboot files."${erase_style}


		Output_Off bless --folder /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices --file /Volumes/Preboot/"$preboot_folder"/System/Library/CoreServices/boot.efi --label "$volume_name"
		Output_Off diskutil apfs updatePreboot "$volume_path"
		Output_Off diskutil unmount /Volumes/Preboot
	else
		echo -e $(date "+%b %d %H:%M:%S") ${text_error}"- Your volume is not formatted as APFS."${erase_style}
		exit
	fi
}

End()
{
	echo -e $(date "+%b %d %H:%M:%S") ${text_message}"/ Thank you for using openprebootmedia."${erase_style}
	
	Input_On
	exit
}

Input_Off
Escape_Variables
Parameter_Variables
Path_Variables
Check_Environment
Check_Root
Check_SIP
Check_Installer
Check_Installer_Stucture
Check_Installer_Version
Check_Installer_Support
Input_Volume
Create_Preboot_Media
End